
#ifndef HARDWARE_H
#define HARDWARE_H

#include "types.h"
typedef struct{
    void (*timer_us)(uint16);
    void (*communication_transmit)(uint8*, uint8);
    void (*communication_receive)(uint8*, uint8);
    void (*write_flash)(uint8*, uint8);
    void (*erase_flash)(uint32);
}hardware;

hardware* get_hardware_pointer(void);
#endif 
