ImgManager_status_t ImgManager__add_line(uint8* u8raw_line, uint8 u8line_lenght)
{
    ImgManager_status_t file_status = IMGMANAGER_FILE_OK;
    IntelHex_line_t Ih_line;

    uint8 checksum = IntelHex__is_calcuate_checksum_ok(u8raw_line, u8line_lenght);

    if(checksum != 1)
    {
        file_status = IMGMANAGER_CHECKSUM_NOT_MATCH;
    }
    else
    {
        IntelHex__load_line(u8raw_line, u8line_lenght, &Ih_line);
    }
    return file_status;
}