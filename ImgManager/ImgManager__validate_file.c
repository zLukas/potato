ImgManager_status_t ImgManager__validate_file(void)
{
    ImgManager_status_t validation =  IMGMANAGER_FILE_OK;


    if(ImgManager_image.u32Image_start_address == 0x00)
    {
        validation  = IMGMANAGER_NO_START_ADDRESS;
    }
    else if((ImgManager_image.u32first_adress < POTATO_START_SECTOR 
            && ImgManager_image.u32last_address < POTATO_END_SECTOR)
    || (ImgManager_image.u32first_adress > POTATO_START_SECTOR 
            && ImgManager_image.u32last_address < POTATO_END_SECTOR)
    || (ImgManager_image.u32first_adress < POTATO_END_SECTOR 
            && ImgManager_image.u32last_address > POTATO_END_SECTOR))
    {
        validation = IMGMANAGER_ADDRESS_CLASH;
    }
    return validation;
}