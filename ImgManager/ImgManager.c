#include "ImgManager.h"
#include "IntelHex.h"

/* save number lines that were loaded */
/* store last and first address */
/* keep app start address */
/* validate if image is not in bootloader range */

/* functions:
-  ImgManager__add_new_line -> add new line to ImgManager object 
-  hex line ImgManager__decode_line -> decode raw intel hex line in scope of whole file. 
-  run target -> set start address in reset vector.
-   
*/
ImgManager_Image_t ImgManager_image;


ImgManager_Image_t* ImgManager_get_image(void)
{
    return &ImgManager_image;
}

#include "ImgManager__add_new_line.c"
#include "ImgManager__decode_line.c"
#include "ImgManager__validate_file.c"



/* how to keep track to previous lines ? */
/* how to ackowledge that line has been uploaded */