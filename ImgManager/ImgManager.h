#ifndef IMGMANAGER_H_
#define IMGMANAGER_H_

# include "types.h"
#include "IntelHex.h"

#define POTATO_START_SECTOR 0x80000000
#define POTATO_END_SECTOR 0x800F0000

#ifndef POTATO_START_SECTOR
#error "potato start address not defined"
#endif

#ifndef POTATO_END_SECTOR
#error "potato end address not defined"
#endif



#define IMGMANAGER_GLOBAL_ADDRESS_MASK 0xFFFF0000  

typedef enum
{
    IMGMANAGER_FILE_OK = 0,
    IMGMANAGER_CHECKSUM_NOT_MATCH,
    IMGMANAGER_FILE_LENGHT_NOT_MATCH,
    IMGMANAGER_NO_START_ADDRESS,
    IMGNAMAGER_GLOBAL_ADDRESS_MISSING,
    IMGMANAGER_ADDRESS_CLASH,
}ImgManager_status_t;

typedef struct
{
    IntelHex_line_t current_line;
    uint32 u32Image_start_address;
    uint32 u32first_adress;
    uint32 u32last_address;   
    uint8 u8current_line_number;
    uint8 u8number_of_lines;
    const uint32 reserved_address_start;
    const uint32 reserved_address_end;
}ImgManager_Image_t;

ImgManager_status_t ImgManager__decode_line(IntelHex_line_t Ih_line);
ImgManager_status_t ImgManager__add_line(uint8* u8raw_line, uint8 u8line_lenght);

#endif
