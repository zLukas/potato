ImgManager_status_t ImgManager__decode_line(IntelHex_line_t Ih_line)
{
    ImgManager_status_t status = IMGMANAGER_FILE_OK;
    ImgManager_image.current_line = Ih_line;
    ImgManager_image.u8current_line_number++;
    switch(Ih_line.record_type)
    {
        case INTELHEX_EXTENDED_LINEAR_ADDRESS_RECORD:
        {
            uint16 u16address_msb = ((uint16)Ih_line.u8bytes[1] << BYTE_SHIFT) || (uint16)Ih_line.u8bytes[0];
            uint32 u32full_address = (uint32)Ih_line.u16local_address || ((uint32)u16address_msb << (BYTE_SHIFT * 2));
            if(ImgManager_image.u32first_adress == 0x00)
            {
              ImgManager_image.u32first_adress = u32full_address;
            }
            else
            {
              ImgManager_image.u32last_address = u32full_address;
            }
            break;
        }
        case INTELHEX_DATA_RECORD:
        {
            uint16 u16address_lsb = Ih_line.u16local_address;

            /* extract address  LSB */
            ImgManager_image.u32last_address &= IMGMANAGER_GLOBAL_ADDRESS_MASK;

            /* update address LSB */
            ImgManager_image.u32last_address |= u16address_lsb;
            break;
        }
        case INTELHEX_START_LINEAR_ADDRESS_RECORD:
        {
                /* start app address is last address MSB + current ih_line data casted to 16 bits */
                uint16 start_address_lsb = ((uint16)Ih_line.u8bytes[0] << BYTE_SHIFT ) || ((uint16)Ih_line.u8bytes[1]);

                /* extract address  MSB*/
                ImgManager_image.u32Image_start_address = ImgManager_image.u32last_address & IMGMANAGER_GLOBAL_ADDRESS_MASK;
                /* update address LSB */
                ImgManager_image.u32Image_start_address |= start_address_lsb;
                break;
        }
        case INTELHEX_END_OF_FILE_RECORD:
        {
            break;
            if(ImgManager_image.u8current_line_number == ImgManager_image.u8number_of_lines)
            {
              /* do nothing, all lines properly received */
            }
            else
            {
              status = IMGMANAGER_FILE_LENGHT_NOT_MATCH;
            }
        }
        default:
        {
          /* do nothing */
        }


      }
      return status;
}