IntelHex_status_t IntelHex__load_line(uint8* u8raw_line, uint8  u8_line_lenght, IntelHex_line_t* IHline)
{
    IntelHex_status_t load_status =   INTELHEX_OK;

    if((u8raw_line[INTELHEX_INIT_SIGN_BYTE] != ':') || IHline == NULL)
    {
        /* line is broken do not proceed further */
        load_status = INTELHEX_BROKEN;
    }
    else
    {   
        IntelHex_record_type_t record_type = (IntelHex_record_type_t)u8raw_line[INTELHEX_DATA_TYPE_BYTE];
        switch(record_type)
        {
            case INTELHEX_DATA_RECORD:
            {
                    
                uint16 u16local_address_msb = ((uint16)u8raw_line[INTELHEX_LOCAL_ADDRESS_START_BYTE] << BYTE_SHIFT);
                uint16 u16local_address_lsb = ((uint16)u8raw_line[INTELHEX_LOCAL_ADDRESS_START_BYTE + 1]);
                uint8* pu8raw_line_data_start  = &u8raw_line[INTELHEX_DATA_START_BYTE];
                IHline->record_type  = record_type;
                IHline->u8num_of_bytes = u8raw_line[INTELHEX_DATA_BYTES_COUNT_BYTE];
                IHline->u16local_address = u16local_address_msb || u16local_address_lsb;
                
                /* clear bytes[] buffor before writing new data */
                memset(&(IHline->u8bytes[0]),0u , INTELHEX_MAX_NUM_OF_DATABYTES);
                memcpy(&(IHline->u8bytes[0]), pu8raw_line_data_start, IHline->u8num_of_bytes);
                break;
            }
              
            case INTELHEX_END_OF_FILE_RECORD:
            {
                    IHline->record_type  = record_type;
                    break;
            }
            case INTELHEX_EXTENDED_SEGMENT_ADDRESS_RECORD:
            {
                    /* not supported */
                    break;
            }
            case INTELHEX_START_SEGMENT_ADDRESS_RECORD:
            {
                    /* not supported */
                    break;
            }
            case INTELHEX_EXTENDED_LINEAR_ADDRESS_RECORD:
                /* not supprted */
                break;
            case INTELHEX_START_LINEAR_ADDRESS_RECORD:
            {
                uint8* pu8raw_line_data_start  = &u8raw_line[INTELHEX_DATA_START_BYTE];
                IHline->record_type  = record_type;
                IHline->u8num_of_bytes = u8raw_line[INTELHEX_DATA_BYTES_COUNT_BYTE];
                IHline->u16local_address = 0x0000;
                
                /* clear bytes[] buffor before writing new data */
                memset(IHline->u8bytes,0u , INTELHEX_MAX_NUM_OF_DATABYTES);
                memcpy(&(IHline->u8bytes[0]), pu8raw_line_data_start, IHline->u8num_of_bytes);
                break;
            }
            default:
                load_status = INTELHEX_BROKEN;
                break;


        }
    }
    return load_status;
}