uint8 IntelHex__is_calcuate_checksum_ok(uint8* u8line, uint8 u8line_lenght)
{

    uint8 u8line_actual_checksum =  u8line[u8line_lenght - 1];
    uint8 u8line_calculated_checksum = 0;
    
    /* calculate whole line checksum LSB,
     uint8 overflow is desired here, we need only last LSB value */
    for(uint8  u8byte = INTELHEX_INIT_SIGN_BYTE + 1; u8byte < u8line_lenght; u8byte++){
        u8line_calculated_checksum += u8line[u8byte];
    }

    u8line_calculated_checksum ^= 0xFF; // bitwise XOR 1'st complement
    u8line_calculated_checksum++;      // 2'nd complement

    return (u8line_calculated_checksum == u8line_actual_checksum) ? 1 : 0;
}