#ifndef INTELHEX_H_
#define INTELHEX_H_


#include "types.h"
/* name: IntelHex.h
   description: IntelHex module API.
  */
/* first line char is ':' */
#define INTELHEX_INIT_SIGN_BYTE   0U
#define INTELHEX_DATA_BYTES_COUNT_BYTE  1U
#define INTELHEX_LOCAL_ADDRESS_START_BYTE 2U
#define INTELHEX_DATA_TYPE_BYTE 4U
#define INTELHEX_DATA_START_BYTE 5U

#define INTELHEX_MAX_NUM_OF_DATABYTES  32U

typedef enum
{
    INTELHEX_BROKEN = 0,
    INTELHEX_OK,
}IntelHex_status_t;

typedef enum
{
    INTELHEX_DATA_RECORD = 0x00,
    INTELHEX_END_OF_FILE_RECORD = 0x01,
    INTELHEX_EXTENDED_SEGMENT_ADDRESS_RECORD = 0x02,
    INTELHEX_START_SEGMENT_ADDRESS_RECORD = 0x03,
    INTELHEX_EXTENDED_LINEAR_ADDRESS_RECORD = 0x04,
    INTELHEX_START_LINEAR_ADDRESS_RECORD = 0x05
}IntelHex_record_type_t;

typedef struct
{
    IntelHex_record_type_t record_type;
    uint16 u16global_address;
    uint16 u16local_address;
    uint8 u8num_of_bytes;
    uint8 u8bytes[INTELHEX_MAX_NUM_OF_DATABYTES];
}IntelHex_line_t;

uint8 IntelHex__is_calcuate_checksum_ok(uint8* u8line, uint8 u8line_lenght);
IntelHex_status_t IntelHex__load_line(uint8* u8raw_line, uint8  u8_line_lenght, IntelHex_line_t* IHline);


#endif