/*
 * user_configuration.h
 *
 *  Created on: 6 Jan 2021
 *      Author: Lukas
 */

#ifndef USER_CONFIGURATION_H_
#define USER_CONFIGURATION_H_

#include "types.h"
#define COMMAND_TIMEOUT           10 // ms


// update flash details
//uncomment  the right one memory architecture
#define FLASH_MEMORY_ARCHITECTURE_NAND 1
//#define FLASH_MEMORY_ARCHITECURE_XOR  1

#define FLASH_WRITING_BUFFER_SIZE 32 // bytes
#define APPLICATION_SECTORS       2
static const uint32 application_sectors_addresses[APPLICATION_SECTORS]={
	0xAAAABBBB,
	0xBBBBCCCC
};

#endif /* USER_CONFIGURATION_H_ */
