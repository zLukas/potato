#include "hardware.h"
// place hardware specific functions here

static hardware hardware_functions/*={
		// uncoment this section and
		// place hardware specific functions wit following pattern:
		.write_flash = hardware_write,
		.timer_us = hardware_timer_dealy,
		...
}*/;


/********Public functions********/
hardware* get_hardware_pointer(void){
    return &hardware_functions;
}
