/* name: types.h
   description: basic data types definitions. 
   author: zLukas
*/

typedef unsigned char uint8;
typedef signed char  int8;
typedef unsigned short uint16;
typedef signed short int16;
typedef float float32;
typedef unsigned long  uint32;
typedef signed long int32;
typedef unsigned long long uint64;
typedef signed long long int64;
typedef unsigned char boolean;

#define TRUE  1U
#define FALSE 0U

#define BYTE_SHIFT   8
