/*
 * memory.c
 *
 *  Created on: 6 Jan 2021
 *      Author: Lukas
 */

#include "memory.h"
#include "user_configuration.h"
#include "hardware.h"

static flash_data_buffer writing_buffer={
		.data= {0},
		.size = 0
};

#ifdef FLASH_MEMORY_ARCHITECTURE_XOR
static const uint8 reset_byte = 0xFF;
#elif defined FLASH_MEMORY_ARCHITECTURE_NAND
static const uint8 reset_byte = 0x00;
#else
#error("no flash architecture choosen")
#endif

// public functions
flash_data_buffer* get_writing_buffer(void){
	return &writing_buffer;
}


writing_buffer_status append_flash_buffer(uint8 data){
	if(is_writing_buffer_full()){
		return buffer_full;
	}else{
		writing_buffer.data[writing_buffer.size] = data;
		writing_buffer.size++;
	}
	return buffer_ok;
}

boolean is_writing_buffer_full(void){
	return (writing_buffer.size == FLASH_WRITING_BUFFER_SIZE);
}

void write_buffer_to_flash(void){
	hardware* hardware_interface = get_hardware_pointer();
	hardware_interface->write_flash(writing_buffer.data,writing_buffer.size);
	reset_writing_buffer();

}

void reset_writing_buffer(void){
	for(uint8 i = 0; i < FLASH_WRITING_BUFFER_SIZE - 1; i++){
		writing_buffer.data[i] = reset_byte;
	}
	writing_buffer.size = 0;
}

void erase_flash(void){
	hardware* hardware_interface = get_hardware_pointer();
	for(uint8 i = 0 ; i < APPLICATION_SECTORS; i++){
		hardware_interface->erase_flash(application_sectors_addresses[i]);
	}
}

// private functions

