#ifndef _MEMORY_H
#define	_MEMORY_H

#include "types.h"
#include "user_configuration.h"

typedef struct{
	uint8 data[FLASH_WRITING_BUFFER_SIZE];
	uint8 size;
}flash_data_buffer;

typedef enum{
	buffer_empty= 0,
	buffer_full,
	buffer_ok
}writing_buffer_status;

boolean is_writing_buffer_full(void);
flash_data_buffer* get_writing_buffer(void);
void write_buffer_to_flash(void);
void reset_writing_buffer(void);

#endif /* !_MEMORY_H */
