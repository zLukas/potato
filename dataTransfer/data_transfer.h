
#ifndef DATA_TRANSFER_H
#define DATA_TRANSFER_H

#include "types.h"
#define DATA_TRANSFER_START     0xDD
#define DATA_TRANSFER_END       0xFF
#define DATA_LINE_LENGHT        0xEE
#define RUN_TARGET_CODE         0xAA
#define ERASE_MEMORY_CODE       0xBB
#define ACK                     0xCC
#define SINGLE_BYTE 1


typedef struct{
	uint8* data;
	uint8 size;
}buffer;

void proceed_command(void);

void free_data_buffer(void);

boolean is_hex_transfer_in_progress(void);

buffer get_data_buffer(void);

#endif //DATA_TRANSFER_H
