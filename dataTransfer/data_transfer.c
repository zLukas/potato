#include "types.h"
#include "data_transfer.h"
#include "intel_hex.h"
#include "hardware.h"
#include "error_flags.h"
#include <malloc.h>
#include "stdio.h"

/**** Private variables****/
buffer data_buffer;
uint8 error_byte = 0;

/****Private functions declaration****/
static void send_ack(void);
static void send_error_flags(void);
static void receive_hex_line(void);

/****Public functions****/

void proceed_command(void){
    uint8 command = 0;
    hardware* target = get_hardware_pointer();
    target->communication_receive(&command, SINGLE_BYTE);
    switch(command){
        case RUN_TARGET_CODE:
            run_target();
            break;
        case ERASE_MEMORY_CODE:
            // this command is send if data transfer was interrupted
            //erase_memory()
            break;
        case DATA_TRANSFER_START:{
            receive_hex_line();
            send_ack();
        }
    }
}

buffer get_data_buffer(void){
	return data_buffer;
}
void free_data_buffer(void){
    free(data_buffer.data);
}
/****Private functions****/
static void send_ack(void){
    uint8 response = ACK;
    get_hardware_pointer()->communication_transmit(&response, SINGLE_BYTE);

}
static void receive_hex_line(void){
    hardware* hardware_functions = get_hardware_pointer();
    hardware_functions->communication_receive(&data_buffer.size, SINGLE_BYTE);
    //timeout
    if(data_buffer.size != 0){
        send_ack();
    }else{
        error_byte = (error_byte << DATA_TRANSFER_INTERUPTED);
        send_error_flags();
        return;
    }
    if(data_buffer.size != DATA_TRANSFER_END && data_buffer.size != 0){
    	data_buffer.data = (uint8*) calloc(data_buffer.size, sizeof(uint8));
    	hardware_functions->communication_receive(data_buffer.data, data_buffer.size);
        send_ack();
    }
}

static void send_error_flags(void){
    get_hardware_pointer()->communication_transmit(&error_byte,SINGLE_BYTE);
    error_byte = 0;
}